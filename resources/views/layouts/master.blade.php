
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta name="user-id" content="{{ Auth::user()->id }}">
  <title>DANHIL ENTERPRISES</title>

  <link rel="stylesheet" href="{{ asset('css/app.css') }}">
  <link rel="stylesheet" href="{{ asset('css/custom.css') }}">
  <link rel="stylesheet" href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
  <link href="https://fonts.googleapis.com/css?family=Archivo+Black&display=swap" rel="stylesheet">
  <link rel="icon" href="{{ URL::asset('/img/sewer.png') }}" type="image/x-icon"/>

</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper" id="app">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="/home" class="nav-link text-bold">DANHIL ENTERPRISES</a>
      </li>
    </ul>


    <ul class="navbar-nav ml-auto">
      <div class="btn-group dropleft">
        <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        </button>
        <div class="dropdown-menu">
          <router-link class="dropdown-item" to="/EditProfile">Edit Profile</router-link>
          <button class="dropdown-item" href="#" onclick="logout()"><i class="fas fa-power-off red"></i><span class="red"> Logout</span></button>

          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
          </form> 
        </div>
      </div>
    </ul>
    
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/home" class="brand-link">
      <img src="{{  asset('img/sewer.png') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">DANHIL ENT.</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{ asset('img/boy.png') }}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">{{Auth::user()->firstname}} {{Auth::user()->lastname}}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          
          <li class="nav-item">
            <router-link to="home" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </router-link>
          </li>
          @can(1)
          <li class="nav-item">
            <router-link to="/user" class="nav-link">
              <i class="nav-icon fas fa-users"></i>
              <p>
                User
              </p>
            </router-link>
          </li>
          @endcan
          @can(2)
          <li class="nav-item">
            <router-link to="/unit" class="nav-link">
              <i class="nav-icon fas fa-box-open"></i>
              <p>
                Unit
              </p>
            </router-link>
          </li>
          @endcan
          @can(3)
          <li class="nav-item">
            <router-link to="/customer" class="nav-link">
              <i class="nav-icon fas fa-user-tag"></i>
              <p>
                Customer
              </p>
            </router-link>
          </li> 
          @endcan

          @can(4)
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-boxes"></i>
              <p>
                Inventory
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              
              <li class="nav-item">
                <router-link to="/product" class="nav-link">
                  <span class="nav-icon ml-4"><img style="width:25px;" src="http://bestanimations.com/Signs&Shapes/Arrows/Right/right-arrow-4.gif" alt=""></span>
                  <p>Item</p>
                </router-link>
              </li>

              <li class="nav-item">
                <router-link to="/package" class="nav-link">
                  <span class="nav-icon ml-4"><img style="width:25px;" src="http://bestanimations.com/Signs&Shapes/Arrows/Right/right-arrow-4.gif" alt=""></span>
                  <p>Package</p>
                </router-link>
              </li>

            </ul> 
          </li>
          @endcan

          @can(6)
          <li class="nav-item">
            <router-link to="/expenses" class="nav-link">
           <i class="fas fa-chart-pie  nav-icon"></i>
              <p>
                Expenses
              </p>
            </router-link>
          </li>
          @endcan 

          @can(5)
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-file"></i>
              <p>
                Order
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              
              <li class="nav-item">
                <router-link to="/newOrder" class="nav-link">
                  <span class="nav-icon ml-4"><img style="width:25px;" src="http://bestanimations.com/Signs&Shapes/Arrows/Right/right-arrow-4.gif" alt=""></span>
                  <p>New Order</p>
                </router-link>
              </li>

              <li class="nav-item">
                <router-link to="/orders" class="nav-link">
                  <span class="nav-icon ml-4"><img style="width:25px;" src="http://bestanimations.com/Signs&Shapes/Arrows/Right/right-arrow-4.gif" alt=""></span>
                  <p>Order list</p>
                </router-link>
              </li>

              {{-- <li class="nav-item">
                <router-link to="/listOfOrder" class="nav-link">
                  <span class="nav-icon ml-4"><img style="width:25px;" src="http://bestanimations.com/Signs&Shapes/Arrows/Right/right-arrow-4.gif" alt=""></span>
                  <p>Orders</p>
                </router-link>
              </li> --}}

            </ul> 
          </li>
          @endcan
          @can(7)
          <li class="nav-item">
            <router-link to="/collections" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Collections
              </p>
            </router-link>
          </li> 
          @endcan

          @can(8)
          <li class="nav-item">
            <router-link to="/activity" class="nav-link">
              <i class="fa fa-eye nav-icon" aria-hidden="true" ></i>
              <p>
                Activity Log
              </p>
            </router-link>
          </li> 
          @endcan

        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            {{-- <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Starter Page</li>
            </ol> --}}
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
          <router-view></router-view>
          <vue-progress-bar></vue-progress-bar>
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
      DANHIL ENTERPRISES
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2019-2020 <a href="https://naotech.com.ph/">Naotech Incorporated</a>.</strong> All rights reserved.
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->

<script src="{{ asset('js/app.js') }}"></script>
<script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>

<script>
      function logout(){
        event.preventDefault();
        Swal.fire({
        title: 'Are you sure you want to logout?',
        text: "",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Logout'
      }).then((result) => {
        if (result.value) {
          document.getElementById('logout-form').submit();
        }
      })
      
      }
</script>
</body>
</html>
