@extends('layouts.app')

@section('content')
        <div class="limiter" >
            <div class="container-login100" style="background-image: url('/img/trading.jpg');">
                <div class="wrap-login100 ">
                    <form class="login100-form validate-form"  method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="login100-form-avatar" >
                            <img src="img/sewer.png" alt="AVATAR">
                        </div>

                        <span class="login100-form-title p-t-20 p-b-45">
                            DANHIL ENTERPRISES
                        </span>

                        @if(session()->get('errors'))
                            <span class="alert alert-danger text-bold w-100 text-center">
                                    {{ session()->get('errors')->first() }}  
                            </span>
                        @endif

                        <div class="wrap-input100 validate-input m-b-10" data-validate = "Username is required">
                             
                            <input id="email" placeholder="Email Address" value="super.admin@gmail.com" type="email" class="input100 form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="off" autofocus >
                            <span class="focus-input100"></span>
                            <span class="symbol-input100">
                                <i class="fa fa-user"></i>
                            </span>
                          
                        </div>

                        <div class="wrap-input100 validate-input m-b-10" data-validate = "Password is required">
                            <input id="password" type="password" value="danhil2019" class="input100 form-control @error('email') is-invalid @enderror" name="password" required placeholder="Password">
                            <span class="focus-input100"></span>
                            <span class="symbol-input100">
                                <i class="fa fa-lock"></i>
                            </span>
                          
                        </div>

                        <div class="container-login100-form-btn p-t-10">
                            <button type="submit" class="login100-form-btn form-control">
                                {{ __('Login') }}
                            </button>
                          
                        </div>

                    </form>
                </div>
            </div>
        </div>
@endsection
