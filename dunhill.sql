-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 09, 2020 at 05:27 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dunhill`
--

-- --------------------------------------------------------

--
-- Table structure for table `activity_logs`
--

CREATE TABLE `activity_logs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `causerName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `action` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tableName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `activity_logs`
--

INSERT INTO `activity_logs` (`id`, `causerName`, `action`, `tableName`, `model`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'created', 'fernando', 'user', '2020-07-08 17:15:21', '2020-07-08 17:15:21'),
(2, 'Admin', 'deleted', 'fernando', 'user', '2020-07-08 17:17:48', '2020-07-08 17:17:48'),
(3, 'Admin', 'created', 'adad', 'user', '2020-07-08 17:18:32', '2020-07-08 17:18:32'),
(4, 'Admin', 'deleted', 'adad', 'user', '2020-07-08 17:18:36', '2020-07-08 17:18:36'),
(5, 'Admin', 'created', 'fernando', 'user', '2020-07-08 17:19:21', '2020-07-08 17:19:21'),
(6, 'Admin', 'deleted', 'fernando', 'user', '2020-07-08 17:23:56', '2020-07-08 17:23:56'),
(7, 'Admin', 'deleted', 'samsung', 'product', '2020-07-08 19:07:03', '2020-07-08 19:07:03'),
(8, 'Admin', 'deleted', 'test2', 'product', '2020-07-08 19:07:11', '2020-07-08 19:07:11'),
(9, 'Admin', 'deleted', 'test', 'product', '2020-07-08 19:07:11', '2020-07-08 19:07:11'),
(10, 'Admin', 'created', 'charer', 'product', '2020-07-08 19:30:39', '2020-07-08 19:30:39'),
(11, 'Admin', 'created', 'charer', 'product', '2020-07-08 19:30:41', '2020-07-08 19:30:41'),
(12, 'Admin', 'created', 'earphone', 'product', '2020-07-08 19:30:59', '2020-07-08 19:30:59'),
(13, 'Admin', 'created', 'cases', 'product', '2020-07-08 19:31:19', '2020-07-08 19:31:19'),
(14, 'Admin', 'Deleted', 'test', 'expenses', '2020-07-09 18:07:43', '2020-07-09 18:07:43'),
(15, 'Admin', 'Deleted', 'test', 'expenses', '2020-07-09 18:07:46', '2020-07-09 18:07:46'),
(16, 'Admin', 'Deleted', 'test', 'expenses', '2020-07-09 18:17:51', '2020-07-09 18:17:51'),
(17, 'Admin', 'Deleted', '24', 'expenses', '2020-07-09 19:20:23', '2020-07-09 19:20:23'),
(18, 'Admin', 'Deleted', '13', 'expenses', '2020-07-09 19:20:59', '2020-07-09 19:20:59'),
(19, 'Admin', 'Deleted', 'qwe', 'expenses', '2020-07-09 19:25:07', '2020-07-09 19:25:07'),
(20, 'Admin', 'Deleted', 'test', 'expenses', '2020-07-09 19:25:21', '2020-07-09 19:25:21'),
(21, 'Admin', 'Deleted', '13132', 'expenses', '2020-07-09 19:28:56', '2020-07-09 19:28:56'),
(22, 'Admin', 'Deleted', 'tessf', 'expenses', '2020-07-09 19:35:14', '2020-07-09 19:35:14'),
(23, 'Admin', 'Deleted', 'eqweqw', 'expenses', '2020-07-09 19:36:45', '2020-07-09 19:36:45'),
(24, 'Admin', 'Deleted', '131313', 'expenses', '2020-07-09 19:38:49', '2020-07-09 19:38:49'),
(25, 'Admin', 'Deleted', '123123', 'expenses', '2020-07-09 19:43:29', '2020-07-09 19:43:29'),
(26, 'Admin', 'Deleted', '244', 'expenses', '2020-07-09 19:45:56', '2020-07-09 19:45:56'),
(27, 'Admin', 'Deleted', 'w', 'expenses', '2020-07-09 19:46:34', '2020-07-09 19:46:34'),
(28, 'Admin', 'Deleted', '1000', 'expenses', '2020-07-09 20:13:38', '2020-07-09 20:13:38'),
(29, 'Admin', 'Deleted', '1000', 'expenses', '2020-07-09 20:13:42', '2020-07-09 20:13:42'),
(30, 'Admin', 'Deleted', '1000', 'expenses', '2020-07-09 20:16:47', '2020-07-09 20:16:47'),
(31, 'Admin', 'Deleted', '113', 'expenses', '2020-07-09 20:17:36', '2020-07-09 20:17:36'),
(32, 'Admin', 'Deleted', '1000', 'expenses', '2020-07-09 20:17:59', '2020-07-09 20:17:59'),
(33, 'Admin', 'Deleted', '100', 'expenses', '2020-07-09 20:18:31', '2020-07-09 20:18:31'),
(34, 'Admin', 'Deleted', '1000', 'expenses', '2020-07-09 20:21:03', '2020-07-09 20:21:03'),
(35, 'Admin', 'Deleted', '100', 'expenses', '2020-07-09 20:21:05', '2020-07-09 20:21:05'),
(36, 'Admin', 'Deleted', '19', 'expenses', '2020-07-09 20:24:29', '2020-07-09 20:24:29'),
(37, 'Admin', 'Deleted', '100', 'expenses', '2020-07-09 20:24:31', '2020-07-09 20:24:31'),
(38, 'Admin', 'Deleted', '100', 'expenses', '2020-07-09 20:24:52', '2020-07-09 20:24:52'),
(39, 'Admin', 'Deleted', '100', 'expenses', '2020-07-09 20:30:21', '2020-07-09 20:30:21'),
(40, 'Admin', 'Deleted', '100', 'expenses', '2020-07-09 20:30:23', '2020-07-09 20:30:23'),
(41, 'Admin', 'Deleted', '100', 'expenses', '2020-07-09 20:31:22', '2020-07-09 20:31:22'),
(42, 'Admin', 'Deleted', '100', 'expenses', '2020-07-09 20:42:48', '2020-07-09 20:42:48'),
(43, 'Admin', 'Deleted', '100', 'expenses', '2020-07-09 20:46:51', '2020-07-09 20:46:51'),
(44, 'Admin', 'Deleted', '1000', 'expenses', '2020-07-09 20:46:53', '2020-07-09 20:46:53'),
(45, 'Admin', 'Deleted', '1000', 'expenses', '2020-07-09 20:46:56', '2020-07-09 20:46:56'),
(46, 'Admin', 'Deleted', '100', 'expenses', '2020-07-09 20:46:59', '2020-07-09 20:46:59'),
(47, 'Admin', 'Deleted', '100', 'expenses', '2020-07-09 20:47:01', '2020-07-09 20:47:01'),
(48, 'Admin', 'Deleted', '1000', 'expenses', '2020-07-09 20:55:02', '2020-07-09 20:55:02'),
(49, 'Admin', 'Deleted', '1000', 'expenses', '2020-07-09 20:55:06', '2020-07-09 20:55:06'),
(50, 'Admin', 'Deleted', '1000', 'expenses', '2020-07-09 20:55:08', '2020-07-09 20:55:08'),
(51, 'Admin', 'Deleted', '1000', 'expenses', '2020-07-09 20:55:10', '2020-07-09 20:55:10'),
(52, 'Admin', 'Deleted', '1000', 'expenses', '2020-07-09 20:57:53', '2020-07-09 20:57:53'),
(53, 'Admin', 'Deleted', '1000', 'expenses', '2020-07-09 20:59:01', '2020-07-09 20:59:01'),
(54, 'Admin', 'Deleted', '1000', 'expenses', '2020-07-09 20:59:05', '2020-07-09 20:59:05'),
(55, 'Admin', 'Deleted', '1000', 'expenses', '2020-07-09 21:02:34', '2020-07-09 21:02:34'),
(56, 'Admin', 'Deleted', '100', 'expenses', '2020-07-09 21:03:32', '2020-07-09 21:03:32'),
(57, 'Admin', 'Deleted', '1000', 'expenses', '2020-07-09 21:07:23', '2020-07-09 21:07:23'),
(58, 'Admin', 'Deleted', 'water', 'expenses', '2020-07-09 21:25:52', '2020-07-09 21:25:52'),
(59, 'Admin', 'Deleted', '1000', 'expenses', '2020-07-09 21:28:09', '2020-07-09 21:28:09'),
(60, 'Admin', 'Deleted', 'water', 'expenses', '2020-07-09 21:28:13', '2020-07-09 21:28:13'),
(61, 'Admin', 'Deleted', '100', 'expenses', '2020-07-09 21:31:10', '2020-07-09 21:31:10'),
(62, 'Admin', 'Deleted', '1000', 'expenses', '2020-07-09 21:31:50', '2020-07-09 21:31:50'),
(63, 'Admin', 'Deleted', '1000', 'expenses', '2020-07-09 21:44:57', '2020-07-09 21:44:57'),
(64, 'Admin', 'Deleted', '1000', 'expenses', '2020-07-10 17:23:51', '2020-07-10 17:23:51'),
(65, 'Admin', 'Deleted', '100', 'expenses', '2020-07-10 17:24:01', '2020-07-10 17:24:01'),
(66, 'Admin', 'created', 'sample', 'customer', '2020-07-13 17:57:57', '2020-07-13 17:57:57'),
(67, 'Admin', 'collected', '3000', 'collection', '2020-07-13 18:01:55', '2020-07-13 18:01:55'),
(68, 'Admin', 'returned', '3000', 'collection', '2020-07-13 18:02:08', '2020-07-13 18:02:08'),
(69, 'Admin', 'collected', '1000', 'collection', '2020-07-13 18:02:20', '2020-07-13 18:02:20'),
(70, 'Admin', 'collected', '2000', 'collection', '2020-07-13 18:02:40', '2020-07-13 18:02:40'),
(71, 'Admin', 'returned', '2000', 'collection', '2020-07-13 18:02:48', '2020-07-13 18:02:48'),
(72, 'Admin', 'collected', '3000', 'collection', '2020-07-13 18:02:58', '2020-07-13 18:02:58'),
(73, 'Admin', 'returned', '3000', 'collection', '2020-07-13 18:03:23', '2020-07-13 18:03:23');

-- --------------------------------------------------------

--
-- Table structure for table `addtocart`
--

CREATE TABLE `addtocart` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `customer_id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `addtocart`
--

INSERT INTO `addtocart` (`id`, `customer_id`, `product_id`, `quantity`, `created_at`, `updated_at`) VALUES
(4, 1, 124, 1, '2020-07-13 18:01:13', '2020-07-13 18:01:13');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Product', NULL, NULL),
(2, 'Package', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `collections`
--

CREATE TABLE `collections` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `order_id` int(11) NOT NULL,
  `payment_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `check_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `check_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` double NOT NULL,
  `check_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `collections`
--

INSERT INTO `collections` (`id`, `order_id`, `payment_type`, `check_type`, `check_no`, `value`, `check_date`, `created_at`, `updated_at`, `status`) VALUES
(2, 1, 'Cash', NULL, NULL, 1000, '2020-07-13', '2020-07-13 18:02:20', '2020-07-13 18:02:20', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `customer` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tin` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `credit` double DEFAULT NULL,
  `balance` double DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `customer`, `tin`, `address`, `contact`, `email`, `credit`, `balance`, `created_at`, `updated_at`) VALUES
(1, 'sample', '53455545345', '34465645676', '09061237968', 'fsiapco@gmail.com', 10000, 10000, '2020-07-13 17:57:57', '2020-07-13 18:02:58');

-- --------------------------------------------------------

--
-- Table structure for table `expenses`
--

CREATE TABLE `expenses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double NOT NULL,
  `date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `expenses`
--

INSERT INTO `expenses` (`id`, `name`, `price`, `date`, `created_at`, `updated_at`) VALUES
(104, '1000', 200, '2020-07-09', '2020-07-09 21:45:05', '2020-07-09 21:45:05'),
(107, '100', 100, '2020-08-14', '2020-07-09 21:45:51', '2020-07-09 21:45:51');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_09_04_071127_create_products_table', 2),
(5, '2019_09_06_055132_create_customers_table', 3),
(6, '2019_09_06_071334_add_credit_to_customers_table', 4),
(7, '2019_09_10_064140_create_addtocart_table', 5),
(8, '2019_09_12_050939_create_orders_table', 6),
(10, '2019_09_12_051016_create_order_details_table', 7),
(11, '2019_09_17_050510_create_roles_table', 8),
(12, '2019_09_17_051306_create_roles_table', 9),
(13, '2019_09_17_070935_create_positions_table', 10),
(14, '2019_09_18_004539_create_user_details_table', 11),
(15, '2019_09_18_005311_create_user_role_table', 11),
(18, '2019_10_16_160910_create_collections_table', 12),
(19, '2019_10_17_095415_add_status_to_orders_table', 13),
(20, '2019_10_29_135246_create_packages_table', 14),
(21, '2019_10_31_125333_create_package__details_table', 15),
(22, '2019_11_04_141339_add_image_to_packages', 16),
(23, '2019_11_04_150945_add_items_to_addtocart', 17),
(24, '2019_11_05_084444_create_categories_table', 18),
(25, '2019_11_05_084827_add_items_to_products', 18),
(26, '2019_11_13_111056_create_product_details_table', 19),
(28, '2019_11_18_172249_create_order_package_details_table', 20),
(29, '2019_12_02_135108_create_monthlies_table', 21),
(30, '2020_03_03_102006_create_units_table', 22),
(31, '2020_03_03_110217_create_units_table', 23),
(32, '2016_06_01_000001_create_oauth_auth_codes_table', 24),
(33, '2016_06_01_000002_create_oauth_access_tokens_table', 24),
(34, '2016_06_01_000003_create_oauth_refresh_tokens_table', 24),
(35, '2016_06_01_000004_create_oauth_clients_table', 24),
(36, '2016_06_01_000005_create_oauth_personal_access_clients_table', 24),
(37, '2020_03_10_151257_create_expenses_table', 25),
(38, '2020_03_11_160900_create_activity_log_table', 26),
(39, '2020_03_12_145620_add_login_fields_to_users_table', 27),
(40, '2020_03_19_150303_create_activity_logs_table', 28);

-- --------------------------------------------------------

--
-- Table structure for table `monthlies`
--

CREATE TABLE `monthlies` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sale` double DEFAULT NULL,
  `expenses` double DEFAULT NULL,
  `profit` double DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `monthlies`
--

INSERT INTO `monthlies` (`id`, `sale`, `expenses`, `profit`, `created_at`, `updated_at`) VALUES
(19, 3000, 200, -8000, '2020-07-09 04:00:00', '2020-07-13 18:01:00'),
(20, NULL, 100, NULL, '2020-08-14 04:00:00', '2020-07-10 17:24:01');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(3, NULL, 'Laravel Personal Access Client', 'CqJbm9FpCHCjKhOTnzbWe64Y6fmAmlTGlEqy4utk', 'http://localhost', 1, 0, 0, '2020-03-05 03:05:33', '2020-03-05 03:05:33'),
(4, NULL, 'Laravel Password Grant Client', 'OKHpRnAzmJBRKpFjKnuKBOfscGSCfngFB50rCuIA', 'http://localhost', 0, 1, 0, '2020-03-05 03:05:33', '2020-03-05 03:05:33'),
(5, NULL, 'Laravel Personal Access Client', '1EciSruKzfXm7wRPbWFPfKnWNs9scdcaAU0KYsFv', 'http://localhost', 1, 0, 0, '2020-03-19 20:50:36', '2020-03-19 20:50:36'),
(6, NULL, 'Laravel Password Grant Client', 'GXLXio8IKDmfw1vDOgkCDVfh0BTaK559A4j8FdCj', 'http://localhost', 0, 1, 0, '2020-03-19 20:50:36', '2020-03-19 20:50:36');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-03-04 03:43:24', '2020-03-04 03:43:24'),
(2, 3, '2020-03-05 03:05:33', '2020-03-05 03:05:33'),
(3, 5, '2020-03-19 20:50:36', '2020-03-19 20:50:36');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `price` double NOT NULL,
  `term` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sales_man` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `unit` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pack` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kart_quantity` int(11) DEFAULT NULL,
  `balance` int(11) NOT NULL,
  `subtotal` double DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `customer_id`, `price`, `term`, `sales_man`, `unit`, `pack`, `kart_quantity`, `balance`, `subtotal`, `created_at`, `updated_at`, `status`) VALUES
(1, 4, 1, 3000, 'COD', 'enan', 'Set', 'Karton', 1, 3000, 2000, '2020-07-13 18:00:59', '2020-07-13 18:03:23', 'unpaid');

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

CREATE TABLE `order_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_details`
--

INSERT INTO `order_details` (`id`, `order_id`, `product_id`, `quantity`, `price`, `created_at`, `updated_at`) VALUES
(1, 1, 124, 1, 1000, '2020-07-13 18:00:59', '2020-07-13 18:00:59'),
(2, 1, 137, 1, 2000, '2020-07-13 18:00:59', '2020-07-13 18:00:59');

-- --------------------------------------------------------

--
-- Table structure for table `order_package_details`
--

CREATE TABLE `order_package_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `package_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE `packages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `package_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `discount` float DEFAULT NULL,
  `total_box` int(11) DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `total_price` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `package__details`
--

CREATE TABLE `package__details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `package_id` bigint(11) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `positions`
--

CREATE TABLE `positions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `position` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `positions`
--

INSERT INTO `positions` (`id`, `position`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2019-09-17 00:37:30', '2019-09-17 00:37:30'),
(2, 'user', '2019-09-17 00:54:54', '2019-09-17 00:54:54');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `item` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `supplier` int(11) DEFAULT NULL,
  `price` double NOT NULL,
  `quantity` int(11) DEFAULT NULL,
  `discount` int(11) NOT NULL DEFAULT 0,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `category_id`, `item`, `supplier`, `price`, `quantity`, `discount`, `photo`, `type`, `created_at`, `updated_at`) VALUES
(123, 1, 'Samsung a90', 15000, 18000, 87, 0, '1580880721.webp', 'item', '2020-02-05 05:32:01', '2020-04-08 20:27:34'),
(124, 1, 'Samsung a50s', 10000, 1000, 43, 0, '1580880798.png', 'item', '2020-02-05 05:33:18', '2020-07-13 18:00:59'),
(136, 2, 'Samsung A Series', 25000, 28900, 2, 10, '1580884410.png', 'package', '2020-02-05 06:33:30', '2020-04-03 03:02:29'),
(137, 1, 'test', 1000, 2000, 10, 0, '1585666126.png', 'item', '2020-04-01 02:48:48', '2020-07-13 18:00:59'),
(138, 1, 'test', 1000, 2000, 12, 0, '1585666141.png', 'item', '2020-04-01 02:49:01', '2020-04-01 02:49:01'),
(142, 2, 'test', 10000, 19500, 2, 10, '1585839819.jpeg', 'package', '2020-04-03 03:03:44', '2020-04-03 03:12:05'),
(143, 2, 'test', 10000, 900, 1, 10, '1585839825.jpeg', 'package', '2020-04-03 03:03:45', '2020-04-03 03:03:45'),
(144, 2, 'test', 30000, 35280, 2, 2, '1586269692.jpeg', 'package', '2020-04-08 02:28:13', '2020-07-02 20:31:39'),
(148, 1, 'charer', 150, 200, 3, 0, '1594193437.png', 'item', '2020-07-08 19:30:39', '2020-07-08 19:30:39'),
(149, 1, 'charer', 150, 200, 3, 0, '1594193440.png', 'item', '2020-07-08 19:30:41', '2020-07-08 19:30:41'),
(150, 1, 'earphone', 100, 150, 2, 0, '1594193458.png', 'item', '2020-07-08 19:30:59', '2020-07-08 19:30:59'),
(151, 1, 'cases', 100, 150, 4, 0, '1594193479.png', 'item', '2020-07-08 19:31:19', '2020-07-08 19:31:19');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `role`, `created_at`, `updated_at`) VALUES
(1, 'User', '2019-09-16 16:00:00', '2019-09-16 16:00:00'),
(2, 'Unit', NULL, NULL),
(3, 'Customer', '2019-09-16 16:00:00', '2019-09-16 16:00:00'),
(4, 'Product', '2019-09-16 16:00:00', '2019-09-16 16:00:00'),
(5, 'New_Order', '2019-09-16 16:00:00', '2019-09-16 16:00:00'),
(6, 'expenses', NULL, NULL),
(7, 'collection', NULL, NULL),
(8, 'Activity_Log', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `units`
--

CREATE TABLE `units` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `units`
--

INSERT INTO `units` (`id`, `name`, `created_at`, `updated_at`) VALUES
(13, 'Piece', '2020-03-04 01:31:35', '2020-03-04 01:31:35'),
(14, 'Pieces', '2020-03-04 01:31:42', '2020-03-04 01:31:42'),
(15, 'Box', '2020-03-04 01:31:47', '2020-03-04 01:31:47'),
(16, 'Boxes', '2020-03-04 01:31:52', '2020-03-04 01:31:52'),
(17, 'Set', '2020-03-04 01:31:58', '2020-03-04 01:31:58'),
(18, 'Sets', '2020-03-04 01:32:03', '2020-03-12 02:53:21');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `firstname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'deactivated',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `last_login_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `firstname`, `lastname`, `email`, `password`, `user_type`, `status`, `created_at`, `updated_at`, `last_login_at`) VALUES
(4, 'Admin', 'Admin', 'super.admin@gmail.com', '$2y$10$KWz9OUmzXWc20.QqaJksUekgJGdYPXsghEyDmP.ytgHxD3X1s6QXi', 'admin', 'activated', '2020-01-07 03:32:08', '2020-08-07 20:51:37', '2020-08-07 16:51:37'),
(13, 'user', 'user', 'user@user.com', '$2y$10$ryKRGGuD7y.S7p6Sid8VrukXGyuJ56fLaOzWee0B7rxgH23PLBaTS', 'user', 'activated', '2020-03-04 09:43:50', '2020-03-04 09:43:53', NULL),
(14, 'stum', 'summit', 'summit@city', '$2y$10$hkXdhHDpNBml8L46A.t2L.LK1bOpdUsPWMSpIzxGLsi7L2Wqk5TsW', 'user', 'activated', '2020-03-04 09:53:24', '2020-03-09 08:46:09', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_details`
--

CREATE TABLE `user_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_details`
--

INSERT INTO `user_details` (`id`, `user_id`, `address`, `contact`, `created_at`, `updated_at`) VALUES
(6, 1, 'Pasig City', '12345678', '2019-11-08 02:15:52', '2019-12-09 06:45:04'),
(7, 4, 'test', '123', '2020-01-07 03:32:08', '2020-03-11 08:24:16'),
(8, 5, 'bulacan', '09466353555', '2020-01-09 05:23:38', '2020-03-04 05:36:35'),
(9, 6, 'Makati', 'asdasd', '2020-01-09 05:24:00', '2020-02-05 05:02:34'),
(10, 7, 'Makati', 'qwewe', '2020-01-09 07:15:43', '2020-02-05 05:02:15'),
(11, 8, 'Makati', 'asd', '2020-01-09 07:18:58', '2020-02-05 05:01:59'),
(12, 9, 'Makati City', '0931231812', '2020-02-05 07:26:53', '2020-02-05 07:26:53'),
(13, 10, 'Makati City', '09231793112', '2020-03-03 03:56:20', '2020-03-03 03:56:20'),
(14, 11, 'Summit', '123121', '2020-03-04 09:30:34', '2020-03-04 09:30:34'),
(15, 12, 'newphone city', '091238916', '2020-03-04 09:31:46', '2020-03-04 09:31:46'),
(16, 13, 'user city', '09213612311', '2020-03-04 09:43:50', '2020-03-04 09:43:50'),
(17, 14, 'sumtra', '09182391', '2020-03-04 09:53:24', '2020-03-04 09:53:24'),
(18, 15, '1312343424', '44243423434', '2020-03-11 08:38:30', '2020-03-12 06:34:11'),
(19, 15, 'angat', '0934545345', '2020-07-02 20:28:04', '2020-07-02 20:28:04'),
(20, 16, '098 marungko', '09061237968', '2020-07-08 17:15:20', '2020-07-08 17:15:20'),
(21, 20, 'asdasd', '242424424', '2020-07-08 17:18:32', '2020-07-08 17:18:32'),
(22, 21, 'marungko,angat,bulacan', '090612379689', '2020-07-08 17:19:21', '2020-07-08 17:19:21');

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE `user_role` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_role`
--

INSERT INTO `user_role` (`id`, `user_id`, `role_id`, `created_at`, `updated_at`) VALUES
(154, 14, 1, '2019-10-04 06:18:00', '2019-10-04 06:18:00'),
(163, 2, 1, '2019-11-08 02:15:52', '2019-11-08 02:15:52'),
(164, 2, 3, '2019-11-08 02:15:52', '2019-11-08 02:15:52'),
(217, 1, 1, '2019-12-09 06:45:04', '2019-12-09 06:45:04'),
(218, 1, 3, '2019-12-09 06:45:04', '2019-12-09 06:45:04'),
(219, 1, 4, '2019-12-09 06:45:04', '2019-12-09 06:45:04'),
(220, 1, 5, '2019-12-09 06:45:04', '2019-12-09 06:45:04'),
(221, 1, 7, '2019-12-09 06:45:04', '2019-12-09 06:45:04'),
(222, 1, 6, '2019-12-09 06:45:04', '2019-12-09 06:45:04'),
(236, 8, 1, '2020-02-05 05:01:59', '2020-02-05 05:01:59'),
(237, 7, 1, '2020-02-05 05:02:15', '2020-02-05 05:02:15'),
(238, 6, 1, '2020-02-05 05:02:34', '2020-02-05 05:02:34'),
(245, 9, 1, '2020-02-05 07:26:53', '2020-02-05 07:26:53'),
(246, 10, 1, '2020-03-03 03:56:20', '2020-03-03 03:56:20'),
(247, 5, 1, '2020-03-04 05:36:35', '2020-03-04 05:36:35'),
(248, 11, 2, '2020-03-04 09:30:34', '2020-03-04 09:30:34'),
(249, 12, 4, '2020-03-04 09:31:46', '2020-03-04 09:31:46'),
(250, 13, 1, '2020-03-04 09:43:50', '2020-03-04 09:43:50'),
(251, 14, 1, '2020-03-04 09:53:24', '2020-03-04 09:53:24'),
(263, 4, 1, '2020-03-11 08:24:16', '2020-03-11 08:24:16'),
(264, 4, 3, '2020-03-11 08:24:16', '2020-03-11 08:24:16'),
(265, 4, 4, '2020-03-11 08:24:16', '2020-03-11 08:24:16'),
(266, 4, 5, '2020-03-11 08:24:16', '2020-03-11 08:24:16'),
(267, 4, 7, '2020-03-11 08:24:16', '2020-03-11 08:24:16'),
(268, 4, 6, '2020-03-11 08:24:16', '2020-03-11 08:24:16'),
(269, 4, 2, '2020-03-11 08:24:16', '2020-03-11 08:24:16'),
(270, 4, 8, '2020-03-11 08:24:16', '2020-03-11 08:24:16'),
(328, 15, 1, '2020-03-12 06:34:11', '2020-03-12 06:34:11'),
(329, 15, 2, '2020-03-12 06:34:11', '2020-03-12 06:34:11'),
(330, 15, 3, '2020-03-12 06:34:11', '2020-03-12 06:34:11'),
(331, 15, 4, '2020-03-12 06:34:11', '2020-03-12 06:34:11'),
(332, 15, 1, '2020-03-12 06:34:11', '2020-03-12 06:34:11'),
(333, 15, 1, '2020-07-02 20:28:04', '2020-07-02 20:28:04'),
(334, 15, 2, '2020-07-02 20:28:04', '2020-07-02 20:28:04'),
(335, 16, 1, '2020-07-08 17:15:21', '2020-07-08 17:15:21'),
(336, 16, 2, '2020-07-08 17:15:21', '2020-07-08 17:15:21'),
(337, 16, 3, '2020-07-08 17:15:21', '2020-07-08 17:15:21'),
(338, 20, 1, '2020-07-08 17:18:32', '2020-07-08 17:18:32'),
(339, 20, 2, '2020-07-08 17:18:32', '2020-07-08 17:18:32'),
(340, 21, 1, '2020-07-08 17:19:21', '2020-07-08 17:19:21'),
(341, 21, 2, '2020-07-08 17:19:21', '2020-07-08 17:19:21');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activity_logs`
--
ALTER TABLE `activity_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `addtocart`
--
ALTER TABLE `addtocart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `collections`
--
ALTER TABLE `collections`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `expenses`
--
ALTER TABLE `expenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `monthlies`
--
ALTER TABLE `monthlies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_details`
--
ALTER TABLE `order_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_package_details`
--
ALTER TABLE `order_package_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `package__details`
--
ALTER TABLE `package__details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `package_id` (`package_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `positions`
--
ALTER TABLE `positions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `units`
--
ALTER TABLE `units`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_details`
--
ALTER TABLE `user_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activity_logs`
--
ALTER TABLE `activity_logs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

--
-- AUTO_INCREMENT for table `addtocart`
--
ALTER TABLE `addtocart`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `collections`
--
ALTER TABLE `collections`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `expenses`
--
ALTER TABLE `expenses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=108;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `monthlies`
--
ALTER TABLE `monthlies`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `order_details`
--
ALTER TABLE `order_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `order_package_details`
--
ALTER TABLE `order_package_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `packages`
--
ALTER TABLE `packages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `package__details`
--
ALTER TABLE `package__details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `positions`
--
ALTER TABLE `positions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=152;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `units`
--
ALTER TABLE `units`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `user_details`
--
ALTER TABLE `user_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `user_role`
--
ALTER TABLE `user_role`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=342;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `package__details`
--
ALTER TABLE `package__details`
  ADD CONSTRAINT `package_details_package_id_foreig` FOREIGN KEY (`package_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
