<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\AddToCart;
use App\Order;
use App\Product;
use App\OrderDetails;
use App\Customer;
use App\OrderPackageDetails;
use App\Collection;
use App\Monthly;
use App\CustomFunctionFolder\CustomFunction;
use Auth;
class OrderController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request , [
            'term' => 'required',
            'salesMan' => 'required',
            'selectedUnit' => 'required',
            'selectedPack' =>'required'
        ]);


        $supplierPrice = [];

        foreach ($request->cartDetails as $key => $value) {
            $supplierPrice[] = ($value['product']['price'] - $value['product']['supplier']) * $value['quantity'];
        }

        $request['term'] = $request->term;
        $request['salesMan'] = $request->salesMan;
        $request['selectedPack'] = $request->selectedPack;
        $request['selectedUnit'] = $request->selectedUnit;
        $addToCart = AddToCart::with(['product' => function($query){
            return $query->with(['packageDetails' => function($query){
                return $query->with('products');
            } ]);
        }])->where('customer_id',$request->customer_id)->get();
        foreach ($addToCart as $value) {
            foreach($value->product->packageDetails as $details){
                OrderPackageDetails::create([
                    'package_id' => $details->package_id,
                    'product_id' => $details->product_id,
                    'quantity' => $details->quantity,
                ]);
            }
        }

        $cart = AddToCart::with('product')->where('customer_id',$request->customer_id)->get();
        $order = Order::create([
            'user_id' => $request->user_id,
            'customer_id' => $request->customer_id,
            'price' => $request->total,
            'term' => $request->term,
            'sales_man' => $request->salesMan,
            'unit' => $request->selectedUnit,
            'pack' =>$request->selectedPack,
            'kart_quantity' => $request->noOfKarton,
            'balance' => $request->total,
            'subtotal' => $request->total,
            'status' => 'unpaid',
        ]);


        $i = 1;

        foreach($cart as $checkOut){
            $order->orderDetails()->create([
                'product_id' => $checkOut->product_id,
                'quantity' => $checkOut->quantity,
                'price' => $checkOut->product->price,
            ]);
                if($checkOut->product->type != "package"){
                    Product::find($checkOut->product->id)->update([
                            'quantity' => $checkOut->product->quantity - $checkOut->quantity
                    ]);
                }else{


                    foreach ($addToCart as  $value) {

                        foreach ($value->product->packageDetails as  $item) {
                            $updateProduct = Product::where('id', $item->product_id)->first();
                            $updateProduct->quantity = $updateProduct->quantity - ($item->quantity * $value->quantity);
                            $updateProduct->save();
                        }
                        if($i++ == 1) break;
                    }
                }
        }
        foreach($cart as $checkOut){
            AddToCart::find($checkOut->id)->delete();
        }
        $this->storeSaleReport($request->total,array_sum($supplierPrice));

        $this->deductCredit($request->customer_id, $request->total);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Order::with('orderDetails.product')->where('customer_id',$id)->orderBy('id','DESC')->first();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function reorder(Request $request){
        $order = Order::with('orderDetails.product')->find($request->order_id);

        $newOrder = Order::create([
            'customer_id' => $order->customer_id,
            'status' => 0,
            'price' => $order->price
        ]);

        foreach($order->orderDetails as $product){
            $newOrder->orderDetails()->create([
                'product_id' => $product->product_id,
                'quantity' => $product->quantity,
                'price' => $product->price
            ]);

            Product::find($product->product->id)->update([
                'quantity' => $product->product->quantity - $product->quantity
            ]);

        }
    }

    public function orderList(Request $request){
        $status = $request->status;
        $filter = $request->filter;

        if($filter != null){
            if($status == 'all'){
                return Order::with('customer','orderDetails','collection')
                    ->where('id','like','%'.$filter. '%')
                    ->latest()
                    ->paginate(5);
            }else{
                return Order::with('customer','orderDetails','collection')
                    ->where('id','like','%'.$filter. '%')
                    ->where('status', $status)
                    ->latest()
                    ->paginate(5);
            }
        }else{
            if($status == 'all'){
                return Order::with('customer','orderDetails','collection')
                    ->latest()
                    ->paginate(5);
            }else{
                return Order::with('customer','orderDetails','collection')
                    ->where('status', $status)
                    ->latest()
                    ->paginate(5);
            }
        }




    }

    public function viewOrderDetails(Request $request){

        $order = Order::with(['user','customer', 'orderDetails' => function($query){
            return $query->with(['product' => function($query){
                return $query->with('orderPackageDetails.products');
            }]);
        }])
        ->find($request->order_id);

        $inv = str_pad($order->id, 8, '0', STR_PAD_LEFT);
        $split = str_split($inv, 4);

        $orNumber = $split[0] . "-" . $split[1];

        return response()->json([
            'orders' => $order,
            'OR' => $orNumber
        ]);
    }

    public function deleteItem(Request $request){
        AddToCart::find($request->id)->delete();
    }


    private function deductCredit($customerId, $total){
        $customer = Customer::find($customerId);
        $customer->balance = $customer->balance - $total;
        if( $customer->balance  <= 0){
            $customer->balance = 0;
        }

        $customer->save();
    }

    private function storeSaleReport($total, $profit){

        $month = date("m");
        $currentMonth = Monthly::whereMonth('created_at',$month)->first();

        if(!isset($currentMonth)){
            Monthly::create([
                'sale' => $total,
                'profit' => $profit
            ]);
        }else{
            $currentMonth->update([
                'sale' => $currentMonth->sale + $total,
                'profit' => $currentMonth->profit + $profit
            ]);
        }
    }

    public function cancelOrder(Request $request){
        // return CustomFunction::updateReport();
        $order = Order::with('orderDetails.product.orderPackageDetails')->find($request->id);

        $this->cancelOrderPay($order->customer_id,$order->price);

        foreach ($order->orderDetails as $key => $value) {

            if($value->product->category_id == 2){
                foreach ($value->product->orderPackageDetails as $key => $item) {
                    $product = Product::where('id',$item->product_id)->first();
                    $product->update([
                        'quantity' => $product->quantity + $item->quantity
                    ]);
                }
            }else{
                $product = Product::where('id',$value->product_id)->first();
                $product->update([
                    'quantity' => $product->quantity + $value->quantity
                ]);
            }
        }

        $order->update([
            'status' => "cancel"
        ]);
    }

    private function cancelOrderPay($customer_id,$price){

        $customer = Customer::find($customer_id);
        $customer->update([
            'balance' => $customer->balance + $price,
        ]);

    }

    public function transactiondetails(request $request){

       $collect =   collection::where('order_id',$request->id)->get();
        $order =  $order = Order::with('orderDetails.product.orderPackageDetails','customer')->find($request->id);
      return response()->json([
            'collect' => $collect,
            'order' => $order
       ]);

    }

}
