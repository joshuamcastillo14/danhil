<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Order;
use App\Customer;
use App\User;
use App\Product;
class DashboardController extends Controller
{
    public function totalOrder(){
        $orders = Order::all()->count();
        $customer = Customer::all()->count();
        $user = User::all()->count();
        $lowProduct = Product::where('quantity','<=',10)->count();
        return $count = [
            'order' => $orders,
            'customer' =>$customer,
            'user' =>$user,
            'product' => $lowProduct

        ];
    }
}
