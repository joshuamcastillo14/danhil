<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Customer;
use App\Collection;
use App\Order;
use App\OrderDetails;
use App\Activity_log;
use Auth;

class CollectionController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    private $status;
    public function index()
    {

        return Collection::with('order.customer')->latest()->paginate(10);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request,[
            'payment_type' => 'required',

        ]);

        $date = $request->check_date;
        $check = substr($date,0,10);
        $request->check_date = $check;
        if($request->payment_type == 'Cash'){
            $this->validate($request,[
                'value' =>'required',
            ]);

            $this->checkBalance($request->order_id, $request->value,$request->payment_type);

        }else{

            $this->validate($request,[
                'check_no' => 'required',
                'check_type' => 'required',
                'value' =>'required',
            ]);
            if($request->payment_type == 'Credit'){
                $request['status'] = 'tobeClear' ;
                $order = Order::find($request->order_id);
                $balance = (int)$order->subtotal - (int)$request->value;

                $order->update([
                    'subtotal' => $balance,
                ]);

                if($balance == 0 ){
                    $order->update([
                        'subtotal' => $balance,
                          'status' => 'pending'
                    ]);
                }else{
                    $order->update([
                        'subtotal' => $balance,
                        // 'status' => 'unpaid'
                    ]);
                }

            }else{
                $request['status'] = 'pending' ;
            }

        }

        $request['payment_type'] =  $request->payment_type;
        $request['check_type'] = $request->check_type;
        $request['check_no'] = $request->check_no;
        $request['check_date'] = $check;
        $request['value'] = $request->value;
        $request['order_id'] = $request->order_id;

        Collection::create($request->only(['order_id','payment_type','check_type','check_no','check_date','value','order_id','status']));
        $this->checkIfPaid($request->order_id,$request->payment_type);
        $this->ActivityLog(Auth::user()->firstname,'collected','collection',$request->value);

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $collection = Collection::where('id',$id)->first();

        $order = Order::with('orderDetails')->find($collection->order_id);

        $this->ActivityLog(Auth::user()->firstname,'returned','collection',$collection->value);
        $updatedBalance = (int)$order->subtotal + (int)$collection->value;
        $cashBalance = (int)$order->balance + (int)$collection->value;

        if($collection->payment_type == 'Cash' ){
            $order->update([
                'balance' => $cashBalance,
                'subtotal' => $updatedBalance,
                'status' => 'unpaid',
            ]);

            $collection->delete();
        }else{
            $order->update([
                'subtotal' => $updatedBalance,
                'status' => 'unpaid',
            ]);
            $collection->delete();

        }

        if($order->balance > 0){
            $order->update([
                'status' => "unpaid"
            ]);
        }



    }


    private function checkBalance($order_id, $amount,$payment_type){

        $order = Order::with('orderDetails')->find($order_id);
        $customer = Customer::find($order->customer_id);
        $subtotal = $order->subtotal - $amount;
        $balance = $order->balance - $amount;
        $customerBalance = $customer->balance + $amount;

        if($balance < 0){
            $balance = 0;
            $customerBalance = $customer->credit;
        }
        if($payment_type == 'Cash'){
            $order->update([
                'balance' => $balance,
                'subtotal' => $subtotal
            ]);
        }else{
            $order->update([
                'balance' => $balance
            ]);
        }


        $customer->update([
            'balance' => $customerBalance,
        ]);







    }

    private function checkIfPaid($order_id,$payment_type){
        $order = Order::find($order_id);

        $collection = Collection::where('order_id',$order_id)->first();

            if($order->subtotal == 0 ){
                if($payment_type === 'Credit'){
                    $order->update([
                        'status' => "pending",
                        'subtotal' => 0,
                    ]);
                }else{
                    if($order->balance <=0){
                        $order->update([
                            'status' => "paid",
                            'subtotal' => 0,
                        ]);
                    }else{
                        $order->update([
                            'status' => "unpaid",
                            'subtotal' => 0,
                            'balance' => 0
                        ]);

                    }

                }
            }
    }

    public function loadCollection(Request $request){

        $status = $request->status;
        $filter = $request->filter;

        if($filter != null){
            if($status == 'all'){
                return Collection::with('order.customer')
                    ->where('order_id','like','%'.$filter. '%')
                    ->latest()
                    ->paginate(5);
            }else{
                return Collection::with('order.customer')
                    ->where('order_id','like','%'.$filter. '%')
                    ->where('payment_type', $status)
                    ->latest()
                    ->paginate(5);
            }
        }else{
            if($status == 'all'){
                return Collection::with('order.customer')
                        ->latest()
                        ->paginate(5);
                }else{
                return Collection::with('order.customer')
                        ->where('payment_type', $status)
                        ->latest()
                        ->paginate(5);
            }
        }

    }

    public function clearCheck($orderID,$collectionID){

        $order = Order::find($orderID);
        $customer = Customer::find($order->customer_id);
        $collection = Collection::where('id',$collectionID)->first();
        $balance = $order->balance - $collection->value ;
        $customerBalance = $customer->balance + $collection->value;

        $customer->update([
            'balance' => $customerBalance,
        ]);

        if($balance <= 0  ){

            $order->update([
                'status' => "paid",
                'balance' => 0,
                'subtotal'=> 0
            ]);

            $collection->update([
                'status' => 'cleared'
            ]);

        }else{

            $order->update([
                'status' => "unpaid",
                'balance' => $balance,

            ]);

            $collection->update([
                'status' => 'cleared'
            ]);



        }


    }


    public function ActivityLog($name,$action,$model,$module){
        return  Activity_log::create([
                'causerName' => $name,
                'action' => $action,
                'model' => $model,
                'tableName' => $module,
        ]);
}



}
