<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Unit;
use App\Activity_log;
use Auth;
class UnitController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Unit::latest()->paginate(10);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required'
        ]);
        return Unit::create([
            'name' => $request['name']
        ]);

        $this->ActivityLog(Auth::user()->firstname,'created','unit',$request->name);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


        $unit = Unit::findOrFail($id);

        $this->validate($request,[
            'name' => 'required|string|max:191'
        ]);

        $unit->update($request->all());
        $this->ActivityLog(Auth::user()->firstname,'updated','unit',$unit->name);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $unit = Unit::findOrFail($id);
        $this->ActivityLog(Auth::user()->firstname,'deleted','unit',$unit->name);
        $unit->delete();
    }


    public function ActivityLog($name,$action,$model,$module){
        return  Activity_log::create([
                'causerName' => $name,
                'action' => $action,
                'model' => $model,
                'tableName' => $module,
        ]);
        }

}
