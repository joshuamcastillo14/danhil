<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Customer;
use DB;
use Auth;
use App\Activity_log;
class CustomerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if(request()->has('newOrder')){
            return Customer::all();
        }else{
            if(isset($_GET['filter'])){
                return DB::table('customers')
                ->where('customer', 'like','%'.$_GET['filter']. '%')
                ->latest()
                ->paginate(5);

            }else{
                return Customer::latest()->paginate(5);
            }

        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request , [
            'customer' => 'required',
            'address' => 'required',
            'contact' => 'required',
            'email' => 'required|email',
            'credit' => 'required'
        ]);
        $request['balance'] = $request->credit;
        Customer::create($request->only(['customer','tin','address','contact','email','credit','balance']));
        $this->ActivityLog(Auth::user()->firstname,'created','customer',$request->customer);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Customer::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request , [
            'customer' => 'required',
            'address' => 'required',
            'contact' => 'required',
            'email' => 'required|email',
            'credit' => 'required'
        ]);
        Customer::find($id)->update($request->all());
        $this->ActivityLog(Auth::user()->firstname,'updated','customer',$request->customer);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $customer = Customer::find($id);
        $this->ActivityLog(Auth::user()->firstname,'deleted','customer',$customer->customer);
        $customer->delete();
    }
    public function search(Request $request){
        $table = $request->table;
        $search = $request->search;
        $column = $request->column;

        return DB::table($table)
                    ->select('*')
                    ->where($column, 'like','%'.$search. '%')
                    ->latest()->paginate(3);
    }

    public function ActivityLog($name,$action,$model,$module){
        return  Activity_log::create([
                'causerName' => $name,
                'action' => $action,
                'model' => $model,
                'tableName' => $module,
        ]);
        }



}
