<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\Package;
use App\Category;
use App\Monthly;
use App\Package_Details;
use Auth;
use App\Activity_log;
class ProductController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    private $fileName = '';
    public function index()
    {
        if(isset($_GET['listProduct'])){
            return Category::with('product')->get();
        }else{
            if(isset($_GET['filter'])){
                return Product::where('item','like','%'.$_GET['filter'].'%')->
                                where('type','item')->latest()->paginate(3);
            }else{
                return Product::where('type','item')->latest()->paginate(3);
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $category = Category::find($request->type);

        $packageSupplier = [];

        if($category->name == 'Package'){

            $this->validate($request , [
                'discount' => 'required',
                
            ]);

            $request['price'] = $request->total_price;
            $request['item'] = $request->package_name;
            $request['type'] = "package";
            $request['supplier'] = 0;

            if($request->imageData != "img/picture.png"){

                $imageData = $request->imageData;

                $name = time().'.' . explode('/', explode(':', substr($imageData, 0, strpos($imageData, ';')))[1])[1];
                \Image::make($imageData)->fit(380, 380)->save(public_path('img/package-images/').$name);
                $request['photo'] = $name;

            }
            if(!isset($request->photo)){
                $request['photo'] = "picture.png";
            }

            foreach($request->listItems as $item){

                $packageSupplier[] = $item['supplier'] * $item['quantity'];
            }

            $request['supplier'] = array_sum($packageSupplier);

        }else{

            $this->validate($request , [
                'item' => 'required',
                'photo' => 'required',
                'quantity' => 'required',
                'supplier' => 'required',
                'price' => 'required',
            ]);



            if(!isset($request->photo)){
                $name = 'picture.png';
            }
            if($request->photo){

                $name = time().'.' . explode('/', explode(':', substr($request->photo, 0, strpos($request->photo, ';')))[1])[1];
                \Image::make($request->photo)->save(public_path('img/product/').$name);
            }

            $request['photo'] = $name;
            $request['type'] = "item";
        }


        $product = $category->product()->create($request->only(['item','supplier','price','discount', 'quantity','photo','type']));

        if($category->name == 'Package'){
            foreach($request->listItems as $listItems){
                Package_Details::create([
                    'package_id' => $product->id,
                    'product_id' => $listItems['id'],
                    'quantity' => $listItems['quantity'],
                ]);
            }
        }


        $this->ActivityLog(Auth::user()->firstname,'created',$product->item ,'product');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Product::with('packageDetails.products')->find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'item' => 'required',
            'price' => 'required',
            'quantity' => 'required'
        ]);
        $product = Product::find($id);

        if($request->photo == $product->photo){
            $name = $request->photo;
        }else{
            $name = time().'.' . explode('/', explode(':', substr($request->photo, 0, strpos($request->photo, ';')))[1])[1];

            \Image::make($request->photo)->save(public_path('img/product/').$name);
        }
        $request['photo'] = $name;
        $product->update($request->only(['item','price','quantity','photo']));

        $this->ActivityLog(Auth::user()->firstname,'updated',$product->item ,'product');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function lowStock(){
        return count(Product::where('quantity','<=','5')
                            ->where('type','item')->get());
    }

    public function deleteProduct(Request $request){
       $product =  Product::find($request->product_id);

        $this->ActivityLog(Auth::user()->firstname,'deleted',$product->item,'product');
        $product->delete();


        $roomPhoto = public_path('/storage/package-images/').$request->photo;
        if(File_exists($roomPhoto)){
            @unlink($roomPhoto);
        }
    }

    public function recent(){
        return Product::where('type','item')->latest('updated_at')->paginate(3);
    }

    public function search_filter(Request $request){
        return Product::where('item','like','%'.$request->keyword.'%')->
                        where('type','item')->latest()->paginate(8);
    }

    public function add_quantity(Request $request){

        $item = Product::find($request->id);
        $item->update([
            'quantity' => $item->quantity + $request->quantity,
            'supplier' => $request->price,
            'price' => $request->saleprice,
        ]);



        return "success";
    }

    public function currentVsList(Request $request){
        $id = $request->id;
        // \DB::enableQueryLog();
        //     $packages_id =  Package_Details::select('product_id')->where('package_id', $id)->get();
            // Product::with('packageDetails')->whereNotIn('id', $packages_id)->where('type', 'item')->get();
            return Product::with('packageDetails')->whereNotIn('id', function($query) use ($id){
                    return $query->select('product_id')
                                ->from('package__details')
                                ->where('package_id', $id);
                })->where('type', 'item')->get();
        // dd(\DB::getQueryLog());
        //
    }


    public function ActivityLog($name,$action,$module,$model){
        return  Activity_log::create([
                'causerName' => $name,
                'action' => $action,
                'model' => $model,
                'tableName' => $module,
        ]);
        }



}
