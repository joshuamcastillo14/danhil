<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use App\User;
use App\UserRole;
use App\UserDetails;
use App\Activity_log;
use Auth;


class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(isset($_GET['filter'])){
            return User::with('user_details')
            ->where('firstname','like','%'.$_GET['filter']. '%')
            ->latest()
            ->paginate(5);
        }else{
            return User::with('user_details')->latest()->paginate(5);

        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request->userRole;
        $this->validate($request,[
            'lastname' => 'required',
            'firstname' => 'required',
            'address' => 'required',
            'email' => 'required|email',
            'contact' => 'required'
        ]);

        $request['password'] = Hash::make("danhil2019");
        $request['user_type'] = 'user';
        $user = User::create($request->only(['lastname','firstname','email','password','user_type']));
        $user->user_details()->create([
            'address' => $request->address,
            'contact' => $request->contact
        ]);

        foreach($request->userRole as $role){
            $user->user_role()->create([
                'role_id' => $role
            ]);
        }

        $this->ActivityLog(Auth::user()->firstname,'created','user',$request->firstname);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return User::with('user_details','user_role')->find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        User::find($id)->update($request->all());
        UserDetails::where('user_id',$id)->update([
            "address" => $request->address,
        ]);
        UserRole::where('user_id',$id)->delete();
        foreach($request->userRole as $role){
            UserRole::create([
                'user_id' => $id,
                'role_id' => $role
            ]);
        }

        $this->ActivityLog(Auth::user()->firstname,'updated','user',$request->firstname);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $this->ActivityLog(Auth::user()->firstname,'deleted','user',$user->firstname);
        $user->delete();
    }

    public function user_details(Request $request){
        return $request->all();
    }
    public function userStatus($id,$status){
        $userStatus = User::with('user_details','user_role')->find($id)->update([
            'status' => $status
        ]);

    }
    public function profile(){
        $user = Auth::user();
        $userDetails = UserDetails::where('user_id',$user->id)->first();

        return response()->json([
            'user' => $user,
            'userDetails' => $userDetails
        ]);
    }
    public function updateProfile(Request $request){
        $user = auth('api')->user();

        $this->validate($request,[
            'lastname' => 'required|string|max:191',
            'firstname' => 'required|string|max:191',
            'address' => 'sometimes|required|string|max:191',
            'email' => 'sometimes|required|string|email|max:191|unique:users,email,'.$user->id,
            'contact' => 'sometimes|required|string|max:191',
            'oldPassword' => 'required',
            'password' => 'required |min:8',
        ]);


        if(Hash::check($request->oldPassword, $user->password)){
            $request['firstname'] = $request->firstname;
            $request['lastname'] = $request->lastname;
            $request['address'] = $request->address;
            $request['email'] = $request->email;
            $request['contact'] = $request->contact;
            $request['password']= Hash::make($request->password);


                    $user->update($request->only([
                    'firstname','lastname','address','email','contact','password'
                    ]));

            return 0;
        }else{
            return 1;
        }

    }

    public function ActivityLog($name,$action,$model,$module){
            return  Activity_log::create([
                    'causerName' => $name,
                    'action' => $action,
                    'model' => $model,
                    'tableName' => $module,
            ]);
    }
}
