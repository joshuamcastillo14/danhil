<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Expenses;
use App\Monthly;
use App\Activity_log;
use Auth;
use Carbon\Carbon;
class ExpensesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Expenses::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'price' =>'required',
            'date' =>'required'
        ]);

        $requestDate = $request->date;
        $date = substr($requestDate,0,10);
        $month = substr($date,5,2);
        $request['name'] = $request->name;
        $request['price'] = $request->price;
        $request['date'] = $date;
        $expenses =   Expenses::create($request->only(['name','price','date' ]));

        $totalExpensesMonth = Expenses::whereMonth('date',$month)->sum('price');
        $this->expensesReport($totalExpensesMonth,$date);
         return $expenses;

        $this->ActivityLog(Auth::user()->firstname,'created','expenses',$request->name);

  ;
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $expenses = Expenses::find($id);
        $this->validate($request,[
            'name' => 'required',
            'price' =>'required',
            'date' =>'required'
        ]);

        $requestDate = $request->date;
        $date = substr($requestDate,0,10);
        $month = substr($date,5,2);

        $request['name'] = $request->name;
        $request['price'] = $request->price;
        $request['date'] = $date;

        $expenses->update($request->all([
            'name','price','date'
        ]));

        $totalExpensesMonth = Expenses::whereMonth('date',$month)->sum('price');

        $this->expensesReport($totalExpensesMonth,$date);

        $this->ActivityLog(Auth::user()->firstname,'updated','expenses',$request->name);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){

        $expenses = Expenses::find($id);

        $requestDate = $expenses->date;
        $date = substr($requestDate,0,10);
        $month = substr($date,5,2);
        $this->ActivityLog(Auth::user()->firstname,'Deleted','expenses',$expenses->name);
        $expenses->delete();

        $totalExpensesMonth = Expenses::whereMonth('date',$month)->sum('price');
        $this->expensesReport($totalExpensesMonth,$date);

    }

    public function expensesReport($totalExpensesMonth,$date){

        $month = substr($date,5,2);

        $user = Monthly::whereMonth('created_at', $month)->get();
        if(count($user) == 0){
        Monthly::create([
                'expenses' => $totalExpensesMonth,
                'created_at' => $date
            ]);
         }else{
            $monthly=  Monthly::whereMonth('created_at', $month)->first();
            $monthly->update([
                    'expenses' => $totalExpensesMonth
                ]);

         }



    }

    public function ActivityLog($name,$action,$model,$module){
        return  Activity_log::create([
                'causerName' => $name,
                'action' => $action,
                'model' => $model,
                'tableName' => $module,
        ]);
        }
}
